package biz.erp.dao;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TestDaoImpl implements TestDao{

    @Autowired
    SqlSession sqlSession;

    public String test() throws Exception{
        return sqlSession.selectOne("test");
    }

}
