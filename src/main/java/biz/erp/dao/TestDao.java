package biz.erp.dao;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TestDao {

   String test() throws Exception;
}
