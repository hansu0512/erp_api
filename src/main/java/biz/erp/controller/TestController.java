package biz.erp.controller;

import biz.common.CommonCode;
import biz.common.response.CommonResponse;
import biz.common.response.DataResponse;
import biz.common.response.ResponseStatus;
import biz.erp.domain.SwaggerVO;
import biz.erp.domain.TestVO;
import io.swagger.annotations.*;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping(value = "/bizErp")
    @ApiOperation(value = "스웨거 EXAMPLE", notes = "Hello Swagger 반환합니다.")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "이름", dataType = "string", paramType = "query", dataTypeClass = String.class, example = "홍길동", required = true),
            @ApiImplicitParam(name = "age",  value = "나이", dataType = "string", paramType = "query", dataTypeClass = String.class, example = "99",    required = true)
    })
    @ApiResponses(value = {@ApiResponse(code = 200, response = TestVO.class, message = CommonCode.SWAGGER_COMMON_200_MESSAGE)})
    public CommonResponse test(String name, String age) {
        TestVO testVO = new TestVO();
        testVO.setAge(age);
        testVO.setName(name);
        return DataResponse.of(ResponseStatus.SUCCESS, testVO);
    }

    @PostMapping(value = "/xss", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "XSS TEST", notes = "XSS 공격 방지를 위한 테스트")
    @ApiResponses(value = {@ApiResponse(code = 200, response = SwaggerVO.class, message = CommonCode.SWAGGER_COMMON_200_MESSAGE)})
    public CommonResponse xss(@RequestBody SwaggerVO parameter) {
        SwaggerVO test = new SwaggerVO();
        test.setIframe(parameter.getIframe());
        test.setLink(parameter.getLink());
        test.setHtml(parameter.getHtml());
        test.setScript(parameter.getScript());
        return DataResponse.of(ResponseStatus.SUCCESS, test);
    }
}
