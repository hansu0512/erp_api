package biz.erp.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TestVO {

    private String name;
    private String age;
}

