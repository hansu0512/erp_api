package biz.erp.domain;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@ApiModel(value = "XSS 테스트", description = "XSS 공격방지 테스트를 위한 생성된 VO")
public class SwaggerVO {

    @ApiModelProperty(name = "iframe", value = "iframe tag", example = "<iframe>", required = true)
    private String script;

    @ApiModelProperty(name = "html",   value = "html tag",  example = "<div>", required = true)
    private String html;

    @ApiModelProperty(name = "script", value = "script tag", example = "<script>", required = true)

    private String iframe;

    @ApiModelProperty(name = "link",   value = "link tag",  example = "<a href='naver.com'>", required = true)
    private String link;

    @ApiModelProperty(hidden = true)
    private List<SwaggerVO> swaggerVO;

    @Override
    public String toString() {
        return "SwaggerVO{" +
                "script='" + script + '\'' +
                ", html='" + html + '\'' +
                ", iframe='" + iframe + '\'' +
                ", link='" + link + '\'' +
                ", swaggerVO=" + swaggerVO +
                '}';
    }
}
