package biz.config.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorResponse {

    private int code;
    private String message;
    private String errorMessage;

    public ErrorResponse() {
    }

    public ErrorResponse(ResponseStatus responseStatus) {
        this.code = responseStatus.getCode();
        this.message = responseStatus.getMessage();
    }

    public ErrorResponse(ResponseStatus responseStatus, String errorMessage) {
        this.code = responseStatus.getCode();
        this.message = responseStatus.getMessage();
        this.errorMessage = errorMessage;
    }
}
