package biz.config.exception;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResponseStatus {
    NOT_FOUND(404,"404 PAGE NOT FOUND"),
    ERROR_SYSTEM_ETC(9999, "시스템오류(기타)");

    private int code;
    private String message;

}
