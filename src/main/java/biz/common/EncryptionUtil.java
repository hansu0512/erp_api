package biz.common;

import biz.security.EncryptUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

@Component
public class EncryptionUtil {

	/**
	 * Map 암호화
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Object> setEncryptToMap(Map<String, Object> paramMap) throws Exception {
		EncryptUtils encUtil = new EncryptUtils();
		Map<String, Object> resultMap = new HashMap<>();
		for (Entry<String, Object> entrySet : paramMap.entrySet()) {
			resultMap.put(entrySet.getKey(), encUtil.aesEncrypt((String) entrySet.getValue()));
		}
		return resultMap;
	}
	
	
	/**
	 * Map 복호화
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Object> getDecryptForMap(Map<String, Object> paramMap) throws Exception {
		EncryptUtils encUtil = new EncryptUtils();
		Map<String, Object> resultMap = new HashMap<>();
		for(Entry<String, Object> entrySet : paramMap.entrySet()) {
			resultMap.put(entrySet.getKey(), encUtil.aesDecrypt((String) entrySet.getValue()));
		}
		return resultMap;
	}
	
	/**
	 * String 암호화
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public static String setEncryptToString(String str) throws Exception {
		EncryptUtils encUtil = new EncryptUtils();
		return (String) encUtil.aesEncrypt(str);
	}
	
	/**
	 * String 복호화
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public static String getDecryptForString(String str) throws Exception {
		EncryptUtils encUtil = new EncryptUtils();
		return (String) encUtil.aesDecrypt(str);
	}

}
