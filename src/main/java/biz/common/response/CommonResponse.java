package biz.common.response;

import lombok.Getter;
import lombok.Setter;

import java.net.http.HttpHeaders;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Getter
@Setter
public class CommonResponse {

    private String code;
    private String message;
    private String timestamp;
    private Map<String, Object> data;

    public CommonResponse(ResponseStatus responseStatus) {
        this.code = responseStatus.getCode();
        this.message = responseStatus.getMessage();
        this.timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));
    }

    public CommonResponse(ResponseStatus responseStatus, Map<String, Object> data){
        this.code = responseStatus.getCode();
        this.message = responseStatus.getMessage();
        this.timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));
        this.data = data;
    }

    public CommonResponse(ResponseStatus responseStatus, String message) {
        this.code = responseStatus.getCode();
        this.message = message;
        this.timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));
    }

    public CommonResponse(ResponseStatus responseStatus, String message, HttpHeaders httpHeaders) {
        this.code = responseStatus.getCode();
        this.message = message;
        this.timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));
    }

    public static CommonResponse of(ResponseStatus responseStatus) {
        return new CommonResponse(responseStatus);
    }

    @Override
    public String toString() {
        return "CommonResponse{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}
