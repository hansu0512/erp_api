package biz.common.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResponseStatus {
    SUCCESS("0000", "정상"),
    ERROR_REQUIRED_PARAM("9000", "필수 파라미터 누락"),
    NO_CONTENT("9001", "내용오류"),
    SYSTEM_ERROR("9999", "시스템 오류(기타)");

    private String code;
    private String message;
}
