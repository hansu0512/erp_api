package biz.common;

public class CommonCode {

    public static final String SWAGGER_COMMON_200_MESSAGE = "{" +
                                                              "<br/>&nbsp;&nbsp;code&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 응답코드(TEXT)" +
                                                              "<br>&nbsp;&nbsp;data&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 응답데이터(JSON)</br>" +
                                                              "&nbsp;&nbsp;message&nbsp;&nbsp;  : 응답메세지 (TEXT)<br/>" +
                                                              "&nbsp;&nbsp;timestamp : 응답시간 (TEXT)<br/>" +
                                                              "}";
}
